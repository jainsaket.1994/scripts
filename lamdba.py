import json
import datetime
import urllib
import boto3
from urllib.request import urlopen
def backup(event, context):
    #LOGIC
    client = boto3.client('ec2')
    #Find instance ID of OpenVPN server
    response = client.describe_instances(
        Filters=[
            {
                'Name': 'tag-key',
                'Values': [
                    'openvpn'
                ]
            },
        ]
    )
    openvpn_instance_id = response['Reservations'][0]['Instances'][0]['InstanceId']
    #DateTime for new AMI image
    create_time = datetime.datetime.now()
    create_fmt = create_time.strftime('%Y-%m-%d')
    #Save AMI Image
    backup_image_creation = client.create_image(
        InstanceId='%s' % (openvpn_instance_id),
        Name="OpenVPN_%s_%s" % (openvpn_instance_id, create_fmt),
        Description="OpenVPN_%s" % (create_fmt),
        NoReboot=True,
        DryRun=False
    )
    # Getting Images into list ami_images
    ami_images = client.describe_images(
        Filters=[
            {
                'Name': 'description',
                'Values': [
                    'OpenVPN*'
                ]
            },
        ])
    if len(ami_images['Images'])>= 1:
        # Calculating date that is 30 days from now
        unformatted_date = datetime.datetime.now() - datetime.timedelta(days=30)
        formatted_date = unformatted_date.strftime('%Y-%m-%d')
        # Getting the name of the image into mystr variable for parsing the creation date 
        mystr = ami_images["Images"][-1]["Name"]
        # Getting imageid for finding snapshot 
        ami_id = ami_images["Images"][-1]["ImageId"]
        # Parsing the creation time of the ami image
        unformatted_oldest_image_date_time = datetime.datetime.strptime(mystr[-10:],'%Y-%m-%d')
        formatted_oldest_image_date_time =  datetime.datetime.strftime(unformatted_oldest_image_date_time,'%Y-%m-%d')
        # Deregestering the image and deleting the snapshot if the creation date is older than 30 days
        if formatted_oldest_image_date_time < formatted_date:
            deregister = client.deregister_image(
            ImageId='%s' % ami_id,
            DryRun=False
            )
            snapshots = client.describe_snapshots(
                    DryRun=False,
                    Filters=[
                        {
                            'Name': 'description',
                            'Values': [
                                '*%s*' % ami_id,
                            ]
                        },
                    ]
            )
            deleted_snapshot = client.delete_snapshot(
                    SnapshotId='%s' % (snapshots['Snapshots'][0]['SnapshotId']
    	            )
            )
    return "The END.